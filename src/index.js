import app from './app'
import initialDB from './db/db'

app.listen(3001, () => {
  console.log('server on port 3001')
})

initialDB()
