import routesUser from './routes/user'
import routesTransaction from './routes/transaction'
import routesSesion from './routes/sesion'
import express, { json } from 'express'
import morgan from 'morgan'
import cors from 'cors'
const app = express()

// Middleware
app.use(morgan('dev'))
app.use(json())

app.use(cors())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*') // update to match the domain you will make the request from
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
  next()
})

// Routes
app.use('/login', routesSesion)
app.use('/users', routesUser)
app.use('/transactions', routesTransaction)

export default app
