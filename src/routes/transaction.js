import { transactionAll, addTransaction } from '../controllers/transaction'
import { Router } from 'express'
const router = Router()

router.get('/', transactionAll)
router.post('/', addTransaction)

export default router
