import { Router } from 'express'
import { userAll, userAdd, userDelete, userEdit } from '../controllers/user'
import { validateToken, validateTokenAdmin } from '../middleware/token'
const router = Router()

router.get('/', validateToken, userAll)
router.post('/', userAdd)
router.delete('/:id', validateToken, validateTokenAdmin(['admin']), userDelete)
router.patch('/:id', validateToken, userEdit)

export default router
