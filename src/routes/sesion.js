import { login } from '../controllers/sesion'
import { Router } from 'express'
const router = Router()

router.post('/', login)

export default router
