import { connect } from 'mongoose'
import { config } from 'dotenv'
config()

const db = () => {
  connect(
    process.env.MONGODB_URI,
    {
      keepAlive: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    },
    (err) => {
      if (err) console.log('DB CONECTION ERROR')
      else console.log('DB CONECTED')
    })
}

export default db
