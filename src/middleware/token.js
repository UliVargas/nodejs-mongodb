import { config } from 'dotenv'
import { sign, verify } from 'jsonwebtoken'
import User from '../models/User'
config()

export const generateToken = async (user) => {
  return sign({ user: user._id, role: user.role }, process.env.SECRET)
}

export const verifyToken = async (token) => {
  try {
    return verify(token, process.env.SECRET)
  } catch (error) {
    return null
  }
}

export const validateToken = async (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1]
  const data = await verifyToken(token)
  if (token && data) next()
  else res.status(403).json({ msg: 'access denied' })
}

export const validateTokenAdmin = (roles) => async (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1]
  const data = await verifyToken(token)
  const user = await User.findById(data.user)

  if (roles.includes(user.role)) next()
  else res.status(403).json({ msg: 'access denied' })
}
