import Transaction from '../models/Transaction'
import User from '../models/User'

export const transactionAll = async (req, res) => {
  const transactions = await Transaction.find().lean()
  res.status(200).json(transactions)
}

export const addTransaction = async (req, res) => {
  const transaction = Transaction(req.body)
  await transaction.save()
  await User.findByIdAndUpdate(req.body.userId, { $push: { transactions: transaction._id } })
  res.status(200).json(transaction)
}
