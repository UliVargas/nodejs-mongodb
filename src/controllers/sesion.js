import User from '../models/User'
import { compare } from 'bcryptjs'
import { generateToken } from '../middleware/token'

export const login = async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })
  if (user) {
    const validate = await compare(password, user.password)
    if (validate) {
      const token = await generateToken(user)
      await user.updateOne({ token })
      res.status(200).json({ token, msg: 'Se inició sesión correctamente' })
    } else res.status(403).json({ msg: 'El correo o la contraseña son inocrrectos' })
  } else res.status(403).json({ msg: 'The email entered is not associated with any user' })
}
