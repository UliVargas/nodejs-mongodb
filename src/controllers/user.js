import User from '../models/User'
import bcrypt from 'bcryptjs'
import { userAddSchema } from '../validations/user'

export const userAll = async (req, res) => {
  const users = await User.find().lean()
  res.status(200).json(users)
}

export const userAdd = async (req, res) => {
  const { name, password, email, phone } = req.body
  const { error } = userAddSchema.validate(req.body)

  if (!error) {
    const hash = await bcrypt.hash(password, 10)
    const user = await User.exists({ email })

    if (!user) {
      const addUser = User({ name, email, phone, password: hash })
      const user = await addUser.save()
      res.status(201).json(user)
    } else res.status(403).json({ msg: 'the email already exists in the database' })
  } else {
    res.status(403).json(error.details)
  }
}

export const userDelete = async (req, res) => {
  const { id } = req.params
  const userDelete = await User.findByIdAndDelete(id)
  res.status(201).json({ msg: 'El usuario se eliminó correctamente', userDelete })
}

export const userEdit = async (req, res) => {
  const { id } = req.params
  const { name, email, password, phone, role, balance } = req.body

  if (password) {
    const hash = await bcrypt.hash(password, 10)
    const userEdited = await User.findByIdAndUpdate(id, { name, email, password: hash, phone, role, balance })
    res.status(201).json(userEdited)
  } else {
    const userEdited = await User.findByIdAndUpdate(id, { name, email, phone, role, balance })
    res.status(201).json(userEdited)
  }
}
