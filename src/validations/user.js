import Joi from 'joi'

export const userAddSchema = Joi.object({
  name: Joi.string().min(4).required(),
  password: Joi.string().min(5).required(),
  phone: Joi.string().min(10).required(),
  email: Joi.string().email().required()
})
