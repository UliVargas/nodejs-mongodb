import { model, Schema } from 'mongoose'

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  phone: {
    type: Number,
    required: true
  },
  balance: {
    type: Number,
    default: 0
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  transactions: {
    type: Array,
    default: []
  },
  role: {
    type: String,
    default: 'user'
  },
  created: {
    type: Date,
    default: Date.now
  },
  token: {
    type: String
  }
}, {
  timestamps: false,
  versionKey: false
})

const User = model('User', userSchema)

export default User
