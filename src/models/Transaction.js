import { model, Schema } from 'mongoose'

const transactionSchema = new Schema({
  total: {
    type: Number,
    required: true
  },
  parking: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  ticket: {
    type: Number,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
})

const Transaction = model('Transaction', transactionSchema)

export default Transaction
